from . import config, oauth


def get_user(raw_token):
    base = config.url()
    url = f"{base}/api/v2.3.0/users/current"
    resp = oauth.supla.get(url=url, token=raw_token)
    return resp.json()


def get_sensors(user):
    base = config.url()
    url = f"{base}/api/v2.3.0/channels?io=input"
    resp = oauth.supla.get(url=url, token=user["token"])
    return resp.json()


def get_readouts(user, sensor, limit):
    base = config.url()
    url = f"{base}/api/v2.3.0/channels/{sensor}/measurement-logs?limit={limit}"
    resp = oauth.supla.get(url=url, token=user["token"])
    return resp.json()
