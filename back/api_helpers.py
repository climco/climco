from flask import session, request
from typing import Optional, Any
from . import db


def err(msg, logout=False):
    return {"err": True, "logout": logout, "decription": msg}


def requires_login(callback):
    """
    Use this to authorize user before handling the request. Function must take
    `user` as the argument (or kwarg).
    """

    def wrapper(*args, **kwargs):
        user = get_user()
        if user is None:
            return err("This action requires login", True), 403
        return callback(*args, user=user, **kwargs)

    # This line is needed because Flask view functions are name-sensitive
    # https://stackoverflow.com/q/17256602
    wrapper.__name__ = callback.__name__
    return wrapper


def get_user() -> Optional[Any]:
    if user_key := get_user_key():
        return db().table("users").get(user_key).run()


def get_user_key() -> Optional[int]:
    if user_key := session.get("user_key"):
        return user_key

    if token := request.headers.get("Authorization"):
        token = remove_prefix(token, "Bearer ")
        r = db()
        matches = list(r.table("tokens").filter(r.row["token"] == token).run())
        if len(matches) == 1:
            return matches[0]["user"]


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix) :]
    return text
