from authlib.integrations.flask_client import OAuth
from authlib.integrations.flask_client.apps import FlaskOAuth2App
from flask import Flask
from .config import Config, load_parameter
from .database import db, init_db
from . import config

oauth = OAuth()  # type: ignore

from .api import bp


def make_app():
    global supla
    app = Flask(__name__)
    app.config.from_object(Config)
    app.secret_key = load_parameter("COOKIE_TOKEN", "default-token")
    app.url_map.strict_slashes = False
    oauth.init_app(app)
    register_client(oauth)
    init_db(db())
    app.register_blueprint(bp)
    return app


def register_client(oauth) -> FlaskOAuth2App:
    base = config.url()
    oauth.register(
        name="supla",
        access_token_url=f"{base}/oauth/v2/token",
        access_token_params=None,
        authorize_url=f"{base}/oauth/v2/auth",
        authorize_params=None,
        api_base_url=base,
        client_kwargs={"scope": "account_r channels_r offline_access"},
    )
    return oauth.supla
