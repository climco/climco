from werkzeug.exceptions import BadRequest
from flask_restx import Namespace, Resource
from flask import request
from werkzeug.utils import secure_filename

from .logic import try_for_errors, FileNotValidException, assign_to_user, push_to_queue
from ..api_helpers import requires_login
from ..storage import get_storage

ns = Namespace("Photos", description="Upload photos for analysis")


@ns.route("/")
class Healthcheck(Resource):
    @requires_login
    def post(self, user):
        try:
            if "file" not in request.files:
                raise BadRequest("No file found in the request")

            file = request.files["file"]
            try_for_errors(file)

            # This assertion is safe as this was checked in try_for_errors
            # It only exists so the code checker won't complain
            assert file.filename is not None

            filename = secure_filename(file.filename)
            id = get_storage().stream_to_file(filename, file.stream)

            assign_to_user(user, id)
            push_to_queue(id)
        except FileNotValidException as e:
            raise BadRequest(str(e))
