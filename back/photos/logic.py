ALLOWED_EXTENSIONS = ["png", "jpg", "jpeg", "bmp"]


class FileNotValidException(BaseException):
    ...


def allowed_file(filename) -> bool:
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def try_for_errors(file) -> None:
    if file.filename == "":
        raise FileNotValidException("Filename is an empty string")
    if file.filename is None:
        raise FileNotValidException("No filename")
    if not file:
        raise FileNotValidException("No file")
    if not allowed_file(file.filename):
        raise FileNotValidException(f"Extension is not one of {ALLOWED_EXTENSIONS}")


def assign_to_user(user, id):
    _ = user
    _ = id


def push_to_queue(id):
    _ = id
