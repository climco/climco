from back import db
from hashlib import sha3_256
from uuid import uuid4


class TokenException(Exception):
    """Calendar Exception"""

    def __init__(self, message):
        super().__init__(message)


def create_sha():
    return sha3_256(uuid4().bytes).hexdigest()


def create_token(user, name):
    r = db()

    duplicates = (
        r.table("tokens")
        .filter(r.row["name"] == name)
        .filter(r.row["user"] == user["id"])
        .run()
    )
    if len(list(duplicates)) != 0:
        raise TokenException("Name is duplicated")

    token = {"name": name, "user": user["id"], "token": create_sha()}
    return {**token, "id": r.table("tokens").insert(token).run()["generated_keys"][0]}


def get_user_by_token(token_sha):
    r = db()
    tokens = r.table("tokens").filter(r.row["token"] == token_sha).run()
    if len(tokens) != 1:
        raise TokenException("Unable to find user by this token")
    return tokens[0]


def get_tokens_for_user(user):
    r = db()
    tokens = list(r.table("tokens").filter(r.row["user"] == user["id"]).run())
    [token_dict.pop("token", None) for token_dict in tokens]
    return tokens


def get_token_by_id(id):
    try:
        r = db()
        token = r.table("tokens").get(id).run()
        if token is None:
            raise TokenException("Unable to delete this token")
        return token
    except NameError:
        raise TokenException("Unable to delete this token")


def delete_token_by_id(id):
    try:
        db().table("tokens").get(id).delete().run()
    except NameError:
        raise TokenException("Unable to delete this token")
