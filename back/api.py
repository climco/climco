from .api_helpers import requires_login
from .config import load_parameter, requires_config
from flask import make_response, session, redirect, jsonify, Blueprint
from . import oauth
from . import api_logic
from .calendar import Calendar
from .misc_api import MiscApi
from .restx import FixedApi
from .api_tokens import ApiTokens
from .photos import Photos


bp = Blueprint("api", __name__)
api = FixedApi(bp, version="1.0", title="Filka.io API")

api.add_namespace(Calendar, path="/calendar")
api.add_namespace(MiscApi, path="/etc")
api.add_namespace(Photos, path="/photos")
api.add_namespace(ApiTokens, path="/tokens")


@bp.route("/login")
@requires_config("SUPLA_OAUTH_REDIRECT_URL")
def login():
    redirect_uri = load_parameter("SUPLA_OAUTH_REDIRECT_URL")
    return oauth.supla.authorize_redirect(redirect_uri)


@bp.route("/auth", methods=["GET"])
def auth():
    """OAuth2 for supla"""
    token = oauth.supla.authorize_access_token()
    session["user_key"] = api_logic.insert_or_update_user(token)
    resp = make_response(redirect("/app/main"))
    # setting cookie for the landing page redirect
    resp.set_cookie("logged", "1")
    return resp


@bp.route("/logout")
def logout():
    session.pop("user_key", None)
    resp = make_response(redirect("/"))
    resp.set_cookie("logged", "0")
    return resp


@bp.route("/sensorList", methods=["GET"])
@requires_login
def print_sensor_list(user):
    """API For reading sensor list"""
    return jsonify(api_logic.get_sensors(user))


@bp.route("/sensorID:<int:sensor_id>/weatherData")
@requires_login
def get_weather_data(sensor_id, user):
    """Transform data received from Supla-Cloud to Apexcharts format"""
    return jsonify(api_logic.get_readouts(sensor_id, 5000, user))


@bp.route("/sensorID:<int:sensorId>/weatherStats/<int:hours>")
@requires_login
def get_weather_stats(hours, sensorId, user):
    """
    API for getting weather stats such as max,min,mean and std values
    (possible to extract other data as pandas describe() method)

    IMPORTANT INFO: The time value is calculated from your last sensor reading.
    Its done for debugging purposes, and its gonna be changed in the future

    TODO: Add reading actual epoch time
    TODO: add corr function from pandas
    """
    return jsonify(api_logic.get_sensor_stats(sensorId, hours, user))
