from typing import List
from json import load
from datetime import date


def get_quote_today():
    return get_quote(date.today())


def get_quote(day: date):
    quotes = get_all_quotes()
    chosen_quote = date_to_num(day, len(quotes))
    return quotes[chosen_quote]


def date_to_num(day: date, limit: int) -> int:
    return hash(day.strftime("%d%m%Y")) % limit


def get_all_quotes() -> List:
    with open("back/files/quotes.json") as quotes:
        return load(quotes)
