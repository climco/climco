from flask_restx.reqparse import RequestParser

new_event_parser = RequestParser()
new_event_parser.add_argument("name", type=str, location="json")
new_event_parser.add_argument("date", type=str, location="json")

update_event_parser = RequestParser()
update_event_parser.add_argument("name", type=str, location="json")
update_event_parser.add_argument("date", type=str, location="json")
update_event_parser.add_argument("id", type=int, required=False, location="json")
