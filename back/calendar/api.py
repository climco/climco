from flask_restx import Namespace, Resource
from werkzeug.exceptions import BadRequest
from http import HTTPStatus
from back.api_helpers import requires_login
from . import dao as calendar
from . import models

ns = Namespace(name="Calendar", description="Calendar events API")


@ns.route("/")
class CalendarRoot(Resource):
    @ns.doc("Get all events")
    @requires_login
    def get(self, user):
        _ = user
        return calendar.get_all_events(), HTTPStatus.OK

    @ns.doc("Create new event")
    @ns.expect(models.new_event_parser)
    @requires_login
    def post(self, user):
        try:
            _ = user
            event = models.new_event_parser.parse_args()
            new_id = calendar.add_event(event)
            return {**event, "event_id": new_id}, HTTPStatus.CREATED
        except calendar.CalendarException as e:
            raise BadRequest(str(e))


@ns.route("/<int:event_id>")
class CalendarEvent(Resource):
    @ns.doc("Get single event")
    @requires_login
    def get(self, user, event_id):
        try:
            _ = user
            return calendar.get_event(event_id), HTTPStatus.OK
        except calendar.CalendarException as e:
            raise BadRequest(str(e))

    @ns.doc("Delete single event")
    @requires_login
    def delete(self, user, event_id):
        try:
            _ = user
            calendar.delete_event(event_id)
            return ("", HTTPStatus.NO_CONTENT)
        except calendar.CalendarException as e:
            raise BadRequest(str(e))

    @ns.doc("Update an event")
    @ns.expect(models.update_event_parser)
    @requires_login
    def post(self, user, event_id):
        try:
            _ = user
            event = models.update_event_parser.parse_args(strict=True)
            return calendar.update_event(event_id, event), HTTPStatus.OK
        except calendar.CalendarException as e:
            raise BadRequest(str(e))
