from .abstract import AbstractStorageClient


class StrojioStorageClient(AbstractStorageClient):
    def file_exists(self, filename):
        return False

    def stream_to_file(self, filename, stream):
        pass

    def get_file(self, filename):
        pass
