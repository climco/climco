from abc import ABC, abstractmethod
from typing import List


class StorageException(Exception):
    pass


class AbstractStorageClient(ABC):
    @staticmethod
    @abstractmethod
    def required_config() -> list[str]:
        """
        Returns list of strings that represent
        needed configuration parameters.
        """
        pass

    @abstractmethod
    def file_exists(self, filename) -> bool:
        """
        Check if file with given filename exists in
        given storage.
        """
        pass

    @abstractmethod
    def stream_to_file(self, filename, stream) -> None:
        """
        Strem bytes to file with given filename.
        """
        pass

    @abstractmethod
    def get_file(self, filename) -> bytes:
        """ """
        pass
