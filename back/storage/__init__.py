from .abstract import AbstractStorageClient, StorageException
from .local import LocalStorageClient
from .storjio import StrojioStorageClient
from ..config import requires_config, load_parameter, require_param
from typing import Type, List


@requires_config("STORAGE_CLIENT")
def get_storage() -> AbstractStorageClient:
    """
    Creates an object of appropraite storage client
    and returns it.
    """
    client_name = load_parameter("STORAGE_CLIENT")
    return _get_client(client_name)()


def _get_available_clients() -> List[Type[AbstractStorageClient]]:
    return [AbstractStorageClient, LocalStorageClient, StrojioStorageClient]


@requires_config("STORAGE_CLIENT")
def _check_required_parameters() -> None:
    client_name = load_parameter("STORAGE_CLIENT")
    client: Type[AbstractStorageClient] = _get_client(client_name)

    for needed_param in client.required_config():
        require_param(needed_param)


def _get_client(name) -> Type[AbstractStorageClient]:
    all_clients = _get_available_clients()
    for c in all_clients:
        if c.__name__ == name:
            return c

    raise StorageException(f"Cannot find storage client with name '{name}'")


# We do that to check every parameter on application start
_check_required_parameters()
