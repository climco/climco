from .abstract import AbstractStorageClient, StorageException
from os.path import exists, join
from back.config import load_parameter


class LocalStorageClient(AbstractStorageClient):
    def __init__(self) -> None:
        self.save_dir = load_parameter("LOCAL_STORAGE_DIR")
        assert self.save_dir is not None

    @staticmethod
    def required_config() -> list[str]:
        return ["LOCAL_STORAGE_DIR"]

    def get_file(self, filename) -> bytes:
        path = join(self.save_dir, filename)  # type: ignore
        with open(path, "rb") as fd:
            return fd.read()

    def file_exists(self, filename):
        return exists(join(self.save_dir, filename))  # type: ignore

    def stream_to_file(self, filename, stream):
        path = join(self.save_dir, filename)  # type: ignore
        if exists(path):
            raise StorageException("File already exists")

        with open(path, "wb+") as fd:
            while True:
                data = stream.read(1024)
                if not data:
                    break
                fd.write(data)
