SHELL := /bin/bash
include .env
export

default: 
	if [[ $OSTYPE == 'darwin'* ]]; then make __mac; else make __linux; fi

__linux:
	scripts/dev.sh docker/linux.docker-compose.yml

__mac:
	scripts/dev.sh docker/mac.docker-compose.yml

build-docker:
	scripts/build-docker.sh

dist-host:
	sudo -E docker-compose -f docker/dist.docker-compose.yml pull
	sudo -E docker-compose -f docker/dist.docker-compose.yml up -d

dist-host-down:
	sudo -E docker-compose -f docker/dist.docker-compose.yml down

setup-dev:
	python3 scripts/check-python.py
	rm -rf venv
	python3 -m venv venv || python3 -m virtualenv venv
	source venv/bin/activate && \
		pip install --upgrade pip \
		pip install -r requirements.txt
	cd front && yarn install
	scripts/venv-warning.sh

nginx-dev:
	SVELTE_MODE=nginx_dev make services -j2

services: flask svelte

flask:
	FLASK_DEBUG=true FLASK_APP=back flask run --reload --host localhost

svelte:
	cd front && yarn dev
