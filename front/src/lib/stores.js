import { writable, readable } from 'svelte/store';

export const sensorStoreID = writable(undefined);
export const statsStore = writable([12]);
export const providedTimeRanges = readable([
	{ value: 3, label: 'last 3hrs' },
	{ value: 6, label: 'last 6hrs' },
	{ value: 12, label: 'last 12hrs' },
	{ value: 24, label: 'last 24hrs' },
	{ value: 48, label: 'last 48hrs' },
	{ value: 24 * 7, label: 'last week' },
	{ value: 24 * 31, label: 'last month' },
	{ value: 3 * 31 * 24, label: 'last 3 months' },
	{ value: 6 * 31 * 24, label: 'last 6 months' }
]);
export const displayedTimeRangesStore = writable([]);
