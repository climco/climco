let options = {
	chart: {
		id: 'dataChart',
		type: 'line',
		zoom: {
			enabled: true,
			autoScaleYaxis: true,
			type: 'x'
		},
		toolbar: {
			offsetX: -50
			// offsetY: 10,
		}
	},
	series: [],
	xaxis: {
		type: 'datetime',
		tickPlacement: 'on'
	},
	yaxis: [
		{
			title: {
				text: 'Relative Humidity [%]'
			},
			opposite: true,
			decimalsInFloat: 0
		},
		{
			title: {
				text: 'Temperature [°C]'
			},
			decimalsInFloat: 0
		}
	],
	noData: {
		text: 'Loading...'
	},
	tooltip: {
		x: {
			format: 'HH:mm dd/MM/yy'
		},
		y: [
			{
				title: {
					formatter: function (val) {
						return val + ' [%]';
					}
				}
			},
			{
				title: {
					formatter: function (val) {
						return val + ' [&#176;C]';
					}
				}
			}
		],
		responsive: [
			{
				breakpoint: 600,
				options: {
					stroke: [1, 1]
				}
			}
		]
	},
	animations: {
		enabled: true,
		easing: 'easeinout',
		speed: 1000,
		animateGradually: {
			enabled: true,
			delay: 150
		},
		dynamicAnimation: {
			enabled: true,
			speed: 350
		}
	},
	stroke: {
		dashArray: [5, 0],
		curve: 'smooth',
		width: 2
	}
};

export default options;
