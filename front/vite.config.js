import { sveltekit } from '@sveltejs/kit/vite';
import { isoImport } from 'vite-plugin-iso-import';

const mode = process.env.SVELTE_MODE;

const common = {
	plugins: [sveltekit(), isoImport()],
	resolve: {
		dedupe: ['@fullcalendar/common']
	},
	optimizeDeps: {
		include: ['@fullcalendar/common']
	}
};

const defaultConfig = {
	...common
};

const distConfig = {
	...common,
	server: {
		port: 3000,
		strictPort: true,
		hmr: false
	}
};

const nginxDevConfig = {
	...common,
	server: {
		host: '127.0.0.1',
		port: 3000,
		strictPort: true,
		hmr: {
			port: 3000,
			protocol: 'ws'
		}
	}
};

const config = (() => {
	switch (mode) {
		case 'nginx_dev':
			return nginxDevConfig;
		case 'dist':
			return distConfig;
		default:
			return defaultConfig;
	}
})();

export default config;
