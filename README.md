<table align="center"><tr><td align="center" width="9999">
<img src="/docker/static/logo.png" align="center" width="150" alt="Project icon">

# [filka.io](https://www.filka.io)

Supla-based app to analyse and control your plants
</td></tr></table>

<table align="center"><tr><td align="left" width="9999">

# About

The app was created to analyze cultivation parameters, as the Supla app does not
meet these requirements.

"Filkajo" means "patch for plants" in Esperanto, "(...) the world's most widely spoken constructed international auxiliary language. Created by the Warsaw-based ophthalmologist L. L. Zamenhof in 1887, it was intended to be a universal second language for international communication, or "the international language" (la lingvo internacia)".

</td></tr></table>


<table align="center"><tr><td align="left" width="9999">

# Goals

1. PlantCV implementation
2. Additional sensors implementation, aside from temperature & humidity 
3. Adding weather stats (already almost implemented using [5545.no by Tim Hårek Andreassen](https://www.5545.no), not styled yet)

## Further goals

- The journal
- Recommended temperature/humidity values based on plants
- Shifting cultivation tips and planning
- Weather forecasts
- Recommendations for growing plants based on weather and user's stats

## Already achieved

- The chart (more or less)
- Statistics (partially)
- Calendar (partially)
- Landing page
- Logout from the app

</td></tr></table>

<table align="center"><tr><td align="left" width="9999">

# Instructions

## 

How to use:

1. Wait until our app is be safe (some1 hacked becaue of us it's our worst nightmare, we need to check everything to be sure)
2. For homepage you can simply visit [filka.io](https://www.filka.io)
3. For local host check out instructions below:

## How to install

Install it by typing these commands into your Linux console:

```bash
  git clone https://gitlab.com/climco/climco.git
  cd climco
  make setup-dev
  . venv/bin/activate
  make # hit CTRL+C to quit
```
---

Open your favorite browser. filka.io is now being hosted on
[https://localhost/](https://localhost/). Before logging in you will have to
add OAuth tokens:

 1. Open supla.org, login into your account, and go to Account > Integrations > My OAuth apps > Register new OAuth application

 2. Into the name and description tabs you can write whatever you want

 3. Into authorization callback URLs insert: 
 ```
 https://localhost/api/auth
```
 4. Click  Register a new OAuth application buttons

 5. Create `backend.conf` in project root folder and put info from the
  Configuration tab in Supla. You can look at `backend.conf.sample` for hints. Please, refer ![to this link for detailed information](https://gitlab.com/climco/climco/-/blob/main/backend.conf.sample)

 6. Restart the application. Hit ctrl+c to quit the app and start it again with
  `make`.

---

The app was tested only under Debian GNU/Linux 11 (bullseye) and right now
under Debian GNU/Linux 12 (bookworm) on Firefox, Qutebrowser and GNU IceCat (sometimes on Ungoogled Chromium, but we really encourage you to use GPL based browsers). 

We don't test on Chrome, as well as we do not test our server on windows (come on, just install Linux, make microsoft small again, stop them from closing OpenAi.

Billy please give back the GPT4, and open your ANS like prof Jarek Duda wanted!

</td></tr></table>
