#!/bin/sh

# DIRNAME=$(dirname "$0")
# if [ "$DIRNAME" != "." ]; then
#   echo "Plese run this script from root of the project"
#   exit
# fi
#

if [ "$VIRTUAL_ENV" = "" ]; then
  scripts/venv-warning.sh
  exit
fi

echo "Server will be launched on https://localhost:443/"

sleep 1

scripts/needs-sudo.sh
sudo docker-compose -f $1 pull
sudo docker-compose -f $1 up &

if [ "$WAIT_FOR_PASSWORD" != "0" ]; then
  sleep 7
fi

make nginx-dev

