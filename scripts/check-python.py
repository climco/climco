#!/bin/python3
import sys

if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    exit(0)

exit(1)
