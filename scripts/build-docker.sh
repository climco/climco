
if [ -z "$DOCKER_BACKEND" ]; then
  echo "Please set \$DOCKER_BACKEND variable"
  exit 1
fi

if [ -z "$DOCKER_FRONTEND" ]; then
  echo "Please set \$DOCKER_FRONTEND variable"
  exit 1
fi

DIRNAME=$(dirname "$0")
cd "$DIRNAME/.." #Main project folder
VERSION=0.0.1 # TODO set this dynamically
BUILD_PATH=.

scripts/needs-sudo.sh

echo -e "\n\nBuilding frontend\n\n"

sudo docker \
  --log-level error \
  build \
  -t $DOCKER_FRONTEND:$VERSION \
  -t $DOCKER_FRONTEND:latest \
  -f docker/front.Dockerfile \
  $BUILD_PATH

echo -e "\n\nBuilding backend\n\n"

sudo docker \
  --log-level error \
  build \
  -t $DOCKER_BACKEND:$VERSION \
  -t $DOCKER_BACKEND:latest \
  -f docker/back.Dockerfile \
  $BUILD_PATH

