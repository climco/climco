FROM node:lts-alpine

COPY front .
ENV SVELTE_MODE=dist
RUN yarn install
RUN yarn build
HEALTHCHECK CMD [ "wget", "--spider", "--tries=1", "http://127.0.0.1:3000/app/healthcheck"]
CMD ["node", "build"]
