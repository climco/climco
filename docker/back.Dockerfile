FROM python:3.10

WORKDIR /opt/

COPY back back
COPY requirements.txt .

RUN pip install -r requirements.txt

ENV FLASK_APP=back
HEALTHCHECK CMD [ "wget", "--spider", "--tries=1", "http://127.0.0.1:5000/etc/healthcheck"]
CMD ["flask", "run", "--host", "0.0.0.0"]
